﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m07
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();

            Console.Write("Lottószámok: ");

            for (int i = 0; i < 6; i++)
            {
                Console.Write(r.Next(1, 46) + " ");
            }

            Console.ReadKey();
        }
    }
}
