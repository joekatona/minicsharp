﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m12
{
    class Program
    {
        static void Main(string[] args)
        {
            string txt = "13,14,22,333,4444";
            string[] szamok = txt.Split(',');

            Console.WriteLine("A számok darabszáma: "+szamok.Length);

            int osszeg = 0;
            foreach (var szam in szamok)
            {
                osszeg += int.Parse(szam);
            }

            Console.WriteLine("A számok összege: " + osszeg);
            double atlag = osszeg / szamok.Length;
            Console.WriteLine("A számok átlaga: " + atlag);

            Console.ReadKey();
        }
    }
}
