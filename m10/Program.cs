﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m10
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] t = new int[4];
            for (int i = 0; i < 4; i++)
            {
                Console.Write("Szám {0}: ",i+1);
                t[i] =  int.Parse( Console.ReadLine() );
            }
            Console.WriteLine( "A számok átlaga: {0}", Math.Round( t.Average(), 2 ) );

            Console.ReadKey();
        }
    }
}
