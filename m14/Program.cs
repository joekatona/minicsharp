﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kérek számokat + és - jelekkel elválasztva egymástól!");
            Console.Write("txt: ");
            string txt = Console.ReadLine();

            string[] szamok = txt.Split('+', '-');

            int muvPoz = 0;
            int osszeg = int.Parse( szamok[0] );
            foreach (var k in txt)
            {
                if( k == '+')
                {
                    muvPoz++;
                    osszeg += int.Parse(szamok[muvPoz]);
                }

                if (k == '-')
                {
                    muvPoz++;
                    osszeg -= int.Parse(szamok[muvPoz]);
                }
            }
            Console.WriteLine("Eredmény: "+osszeg);

            Console.ReadKey();
        }
    }
}
