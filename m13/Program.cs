﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m13
{
    class Program
    {
        static void Main(string[] args)
        {
            string szep;
            int[] sz = new int[6];

            Console.Write("Elválasztó karakter: ");
            szep = Console.ReadLine();

            for (int i = 0; i < 6; i++)
            {
                Console.Write("Szám {0}: ",i+1);
                sz[i] = int.Parse( Console.ReadLine() );
            }

            string input = "";
            for (int i = 0; i < 6; i++)
            {
                if (i > 0) input += "-";
                input += sz[i].ToString();
            }
            Console.WriteLine("Az input változó tartalma: "+input);

            Console.WriteLine("A számok összege: "+sz.Sum());

            Console.ReadKey();
        }
    }
}
