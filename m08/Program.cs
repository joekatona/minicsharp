﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m08
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10, b = 8, c = 4;
            // abc acb bac bca cab cba
            if (a < b && b < c) Console.WriteLine( a+" "+b+" "+c );
            if (a < c && c < b) Console.WriteLine( a+" "+c+" "+b );
            if (b < a && a < c) Console.WriteLine( b+" "+a+" "+c );
            if (b < c && c < a) Console.WriteLine( b+" "+c+" "+a );
            if (c < a && a < b) Console.WriteLine( c+" "+a+" "+b );
            if (c < b && b < a) Console.WriteLine( c+" "+b+" "+a );

            Console.ReadKey();
        }
    }
}
