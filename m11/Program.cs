﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m11
{
    class Program
    {
        static void Main(string[] args)
        {
            string mondat;

            Console.Write("Mondat: ");
            mondat = Console.ReadLine();
            Console.WriteLine();

            string[] szavak = mondat.Split();

            Console.WriteLine("Szövegstatisztika:");
            Console.WriteLine("------------------");
            Console.WriteLine("Szavak száma: "+szavak.Length);
            Console.WriteLine("A mondat hossza: "+mondat.Length);

            int cntA = 0;
            foreach (var k in mondat)
            {
                if (k == 'A' || k == 'a') cntA++;
            }
            Console.WriteLine("Az \"a\" betűk száma: " + cntA);

            Console.ReadKey();
        }
    }
}
